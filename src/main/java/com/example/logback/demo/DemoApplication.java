package com.example.logback.demo;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public static final Logger LOGGER = LoggerFactory.getLogger(DemoApplication.class);

	public static final Integer REPEAT_THE_LOGS = new Integer (500);

	@PostConstruct
	public void demoMulitpleLogs() {
		for (int i=0;i<REPEAT_THE_LOGS;i++) {
			LOGGER.error("RepeatedLog");
		}
	}
}
